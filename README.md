# Pixew Back

Este é o repositório para o back-end do projeto Pixew dos integrantes Pedro Tonini Rosenberg Schneider e Clara Yuki Sano.

## Sobre o projeto

Este projeto consiste em uma plataforma para usuários darem notas e registrarem suas opiniões em formato de reviews para jogos. 

## Tecnologias

O back-end da aplicação será feito utilizando a framework [Ktor](https://ktor.io/).