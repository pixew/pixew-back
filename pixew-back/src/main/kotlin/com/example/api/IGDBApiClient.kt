package com.example.api

import com.api.igdb.apicalypse.APICalypse
import com.api.igdb.exceptions.RequestException
import com.api.igdb.request.*
import com.example.models.Game
import proto.Cover
import proto.Screenshot


class IGDBApiClient(clientId: String, clientSecret: String) {
    init {
        IGDBWrapper.setCredentials(clientId, clientSecret)
    }

    fun searchGames(query: String): List<Game> {
        val fields = "id,name,summary,cover,screenshots"

        // Cria a consulta APICalypse
        val apicalypse = APICalypse()
            .search(query)
            .fields(fields)
            .limit(10)

        println("Requesting with query: $query")
        println("APICalypse Query: $apicalypse")

        return try {
            // Faz a requisição para a API
            val games = IGDBWrapper.games(apicalypse)
            println("Response: $games")

            val coverIds = games.map { it.cover?.id }
            val coverUrls = getCoverUrl(coverIds)

            val screenshotIds = games.map { it.screenshotsList?.firstOrNull()?.id }
            val screenshotUrls = getScreenshotUrl(screenshotIds)

            games.mapIndexed { index, gameResponse ->
                Game(
                    id = gameResponse.id,
                    name = gameResponse.name,
                    summary = gameResponse.summary,
                    coverUrl = coverUrls.getOrNull(index),
                    screenshotUrl = screenshotUrls.getOrNull(index)
                )
            }
        } catch (e: RequestException) {
            e.printStackTrace()
            println("RequestException: ${e.message}")
            emptyList()
        } catch (e: Exception) {
            e.printStackTrace()
            println("Exception: ${e.message}")
            emptyList()
        }
    }

    fun getGameById(gameId: Long): Game? {
        //IGDBWrapper.setCredentials("pls0dgmzbcffvadblxfaifvf3s1e7g", "l0k8wb3t9iyly7zr2dhcsbx6wkgxoa")
        val apicalypse = APICalypse().fields("*").where("id = $gameId")
        println(apicalypse)
        try {
            println("antes do .games")
            val games = IGDBWrapper.games(apicalypse)
            val coverId = games.firstOrNull()?.cover?.id
            val coverUrl = getCoverUrl(coverId)
            val screenshotId = games.firstOrNull()?.screenshotsList?.firstOrNull()?.id
            val screenshotUrl = getScreenshotUrl(screenshotId)
            return games.firstOrNull()?.let { gameResponse ->
                Game(
                    id = gameResponse.id,
                    name = gameResponse.name,
                    summary = gameResponse.summary,
                    coverUrl = coverUrl,
                    screenshotUrl = screenshotUrl
                )
            }
        } catch (e: RequestException) {
            println(e.statusCode)
            return null
        }
    }

    fun getSpecificGames(gameIds: List<Long>): List<Game> {
        val allGames = mutableListOf<Game>()

        for (i in gameIds.indices step 10) {
            val batchIds = gameIds.subList(i, kotlin.math.min(i + 10, gameIds.size))
            val apicalypse = APICalypse().fields("*").where("id = ${batchIds.joinToString(prefix = "(", postfix = ")", separator = ",")}")
            println(apicalypse)

            try {
                val games = IGDBWrapper.games(apicalypse)
                println("JOGOS: $games")

                val coverIds = games.map { it.cover?.id }
                val coverUrls = getCoverUrl(coverIds)

                val screenshotIds = games.map { it.screenshotsList?.firstOrNull()?.id }
                val screenshotUrls = getScreenshotUrl(screenshotIds)

                val mappedGames = games.mapIndexed { index, game ->
                    Game(
                        id = game.id,
                        name = game.name,
                        summary = game.summary,
                        coverUrl = coverUrls.getOrNull(index),
                        screenshotUrl = screenshotUrls.getOrNull(index)
                    )
                }
                allGames.addAll(mappedGames)
            } catch (e: RequestException) {
                println(e.statusCode)
            }
        }
        return allGames
    }

    private fun getCoverUrl(coverId: Long?): String? {
        coverId?.let {
            val apicalypse = APICalypse().fields("url").where("id = $it")
            try {
                val covers = IGDBWrapper.covers(apicalypse)
                println(covers)
                return covers.firstOrNull()?.url?.replace("t_thumb", "t_cover_big")
            } catch (e: RequestException) {
                println(e.statusCode)
            }
        }
        return null
    }

    private fun getCoverUrl(coverIds: List<Long?>): List<String?> {
        val apicalypse = APICalypse().fields("url").where("id = ${coverIds.filterNotNull().joinToString(prefix = "(", postfix = ")", separator = ",")}")
        try {
            val covers = IGDBWrapper.covers(apicalypse)
            val sortedCovers = sortCoverUrls(coverIds, covers)
            return sortedCovers.map { cover ->  cover.url?.replace("t_thumb", "t_cover_big")}
        }
        catch (e: RequestException) {
            println(e.statusCode)
            return emptyList()
        }
    }

    private fun getScreenshotUrl(id: Long?): String? {
        id?.let {
            val apicalypse = APICalypse().fields("url").where("id = $it")
            try {
                val screenshots = IGDBWrapper.screenshots(apicalypse)
                println(screenshots)
                return screenshots.firstOrNull()?.url?.replace("t_thumb", "t_screenshot_big")
            } catch (e: RequestException) {
                println(e.statusCode)
            }
        }
        return null
    }

    private fun getScreenshotUrl(screenshotIds: List<Long?>): List<String?> {
        val apicalypse = APICalypse().fields("url").where("id = ${screenshotIds.filterNotNull().joinToString(prefix = "(", postfix = ")", separator = ",")}")
        try {
            val screenshots = IGDBWrapper.screenshots(apicalypse)
            val sortedScreenshots = sortScreenshotUrls(screenshotIds, screenshots)
            return sortedScreenshots.map { screenshot ->  screenshot.url?.replace("t_thumb", "t_screenshot_big")}
        }
        catch (e: RequestException) {
            println(e.statusCode)
            return emptyList()
        }
    }

    private fun sortCoverUrls(coverIds: List<Long?>, covers: List<Cover>): List<Cover> {
        val sortedCovers = mutableListOf<Cover>()
        for (id in coverIds) {
            if (id == null) continue
            for (cover in covers) {
                if (cover.id == id) {
                    sortedCovers.add(cover)
                }
            }
        }
        return sortedCovers
    }

    private fun sortScreenshotUrls(screenshotIds: List<Long?>, screenshots: List<Screenshot>): List<Screenshot> {
        val sortedScreenshots = mutableListOf<Screenshot>()
        for (id in screenshotIds) {
            if (id == null) continue
            for (screenshot in screenshots) {
                if (screenshot.id == id) {
                    sortedScreenshots.add(screenshot)
                }
            }
        }
        return sortedScreenshots
    }
}
