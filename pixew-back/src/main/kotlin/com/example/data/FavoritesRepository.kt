package com.example.data

import com.example.models.Played
import com.example.tables.FavoritesTable
import com.example.factory.DatabaseFactory.dbQuery
import com.example.models.Favorites
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.select

class FavoritesRepository {

    suspend fun addFavorite(favorite: Favorites): Favorites = dbQuery {
        FavoritesTable.insert {
            it[userId] = favorite.userId
            it[gameId] = favorite.gameId
        }
        favorite
    }

    suspend fun deleteFavorite(userId: Int, gameId: Long): Boolean = dbQuery {
        val deletedRows = FavoritesTable.deleteWhere {
            (FavoritesTable.userId eq userId) and (FavoritesTable.gameId eq gameId)
        }
        deletedRows > 0
    }

    suspend fun getFavoriteByUserId(userId: Int): List<Favorites> = dbQuery {
        FavoritesTable.select { FavoritesTable.userId eq userId }
            .map { Favorites(it[FavoritesTable.userId], it[FavoritesTable.gameId]) }
    }

    suspend fun getFavoriteByGameId(gameId: Long): List<Favorites> = dbQuery {
        FavoritesTable.select { FavoritesTable.gameId eq gameId }
            .map { Favorites(it[FavoritesTable.userId], it[FavoritesTable.gameId]) }
    }

    suspend fun getAllFavorites(): List<Favorites> = dbQuery {
        FavoritesTable.selectAll()
            .map { Favorites(it[FavoritesTable.userId], it[FavoritesTable.gameId]) }
    }
}
