package com.example.data

import com.example.models.Played
import com.example.tables.PlayedTable
import com.example.factory.DatabaseFactory.dbQuery
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.select

class PlayedRepository {

    suspend fun addPlayed(played: Played): Played = dbQuery {
        PlayedTable.insert {
            it[userId] = played.userId
            it[gameId] = played.gameId
        }
        played
    }

    suspend fun deletePlayed(userId: Int, gameId: Long): Boolean = dbQuery {
        val deletedRows = PlayedTable.deleteWhere {
            (PlayedTable.userId eq userId) and (PlayedTable.gameId eq gameId)
        }
        deletedRows > 0
    }

    suspend fun getPlayedByUserId(userId: Int): List<Played> = dbQuery {
        PlayedTable.select { PlayedTable.userId eq userId }
            .map { Played(it[PlayedTable.userId], it[PlayedTable.gameId]) }
    }

    suspend fun getPlayedByGameId(gameId: Long): List<Played> = dbQuery {
        PlayedTable.select { PlayedTable.gameId eq gameId }
            .map { Played(it[PlayedTable.userId], it[PlayedTable.gameId]) }
    }

    suspend fun getAllPlayed(): List<Played> = dbQuery {
        PlayedTable.selectAll()
            .map { Played(it[PlayedTable.userId], it[PlayedTable.gameId]) }
    }
}
