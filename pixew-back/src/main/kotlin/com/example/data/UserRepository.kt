package com.example.data

import com.example.models.User
import com.example.tables.UserDAO
import com.example.factory.DatabaseFactory.dbQuery
import com.example.models.UserCreation
import com.example.tables.UserTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class UserRepository {

    suspend fun addUser(userCreation: UserCreation): User = dbQuery {
        val insertedId = UserTable.insertAndGetId {
            it[userName] = userCreation.userName
            it[displayName] = userCreation.displayName
            it[password] = userCreation.password
        }
        User(insertedId.value, userCreation.userName, userCreation.displayName, userCreation.password)
    }

    suspend fun addUsers(userCreations: List<UserCreation>): List<User> = dbQuery {
        userCreations.map { userCreation ->
            val insertedId = UserTable.insertAndGetId {
                it[userName] = userCreation.userName
                it[displayName] = userCreation.displayName
                it[password] = userCreation.password
            }
            User(insertedId.value, userCreation.userName, userCreation.displayName, userCreation.password)
        }
    }

    suspend fun getUserById(id: Int): User? = dbQuery {
        UserTable.select { UserTable.id eq id }
            .mapNotNull { User(it[UserTable.id].value, it[UserTable.userName], it[UserTable.displayName], it[UserTable.password]) }
            .singleOrNull()
    }

    suspend fun getUserByUsername(username: String): User? = dbQuery {
        UserTable.select { UserTable.userName eq username }
            .mapNotNull { User(it[UserTable.id].value, it[UserTable.userName], it[UserTable.displayName], it[UserTable.password]) }
            .singleOrNull()
    }

    suspend fun updateUser(id: Int, user: UserCreation): Boolean = dbQuery {
        val updatedRows = UserTable.update({ UserTable.id eq id }) {
            it[userName] = user.userName
            it[displayName] = user.displayName
            it[password] = user.password
        }
        updatedRows > 0
    }

    suspend fun deleteUser(id: Int): Boolean = dbQuery {
        val deletedRows = UserTable.deleteWhere { UserTable.id eq id }
        deletedRows > 0
    }

    suspend fun getAllUsers(): List<User> = dbQuery {
        UserTable.selectAll()
            .map { User(it[UserTable.id].value, it[UserTable.userName], it[UserTable.displayName], it[UserTable.password]) }
    }
}
