package com.example.data

import com.example.tables.GameReviewDAO
import com.example.factory.DatabaseFactory.dbQuery
import com.example.models.*
import com.example.tables.GameReviewTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import kotlin.math.roundToInt

class GameReviewRepository {

    suspend fun addGameReview(gameReviewCreation: GameReviewCreation): GameReview = dbQuery {
        val insertedId = GameReviewTable.insertAndGetId {
            it[userId] = gameReviewCreation.userId
            it[gameId] = gameReviewCreation.gameId
            it[rating] = gameReviewCreation.rating
            it[text] = gameReviewCreation.text
            it[likes] = gameReviewCreation.likes
        }
        GameReview(insertedId.value, gameReviewCreation.userId, gameReviewCreation.gameId, gameReviewCreation.rating, gameReviewCreation.text, gameReviewCreation.likes)
    }

    suspend fun addGameReviews(gameReviewCreations: List<GameReviewCreation>): List<GameReview> = dbQuery {
        gameReviewCreations.map { gameReviewCreation ->
            val insertedId = GameReviewTable.insertAndGetId {
                it[userId] = gameReviewCreation.userId
                it[gameId] = gameReviewCreation.gameId
                it[rating] = gameReviewCreation.rating
                it[text] = gameReviewCreation.text
                it[likes] = gameReviewCreation.likes
            }
            GameReview(insertedId.value, gameReviewCreation.userId, gameReviewCreation.gameId, gameReviewCreation.rating, gameReviewCreation.text, gameReviewCreation.likes)
        }
    }

    suspend fun getGameReviewById(id: Int): GameReview? = dbQuery {
        GameReviewTable.select { GameReviewTable.id eq id }
            .mapNotNull { GameReview(it[GameReviewTable.id].value, it[GameReviewTable.userId], it[GameReviewTable.gameId], it[GameReviewTable.rating], it[GameReviewTable.text], it[GameReviewTable.likes]) }
            .singleOrNull()
    }

    suspend fun getAllGameReviews(): List<GameReview> = dbQuery {
        GameReviewTable.selectAll()
            .map { GameReview(it[GameReviewTable.id].value, it[GameReviewTable.userId], it[GameReviewTable.gameId], it[GameReviewTable.rating], it[GameReviewTable.text], it[GameReviewTable.likes]) }
    }

    suspend fun getReviewsByUserId(userId: Int): List<GameReview> = dbQuery {
        GameReviewTable.select { GameReviewTable.userId eq userId }
            .map { GameReview(it[GameReviewTable.id].value, it[GameReviewTable.userId], it[GameReviewTable.gameId], it[GameReviewTable.rating], it[GameReviewTable.text], it[GameReviewTable.likes]) }
    }

    suspend fun getReviewsByGameId(gameId: Long): List<GameReview> = dbQuery {
        GameReviewTable.select { GameReviewTable.gameId eq gameId }
            .map { GameReview(it[GameReviewTable.id].value, it[GameReviewTable.userId], it[GameReviewTable.gameId], it[GameReviewTable.rating], it[GameReviewTable.text], it[GameReviewTable.likes]) }
    }

    suspend fun getNewReviews(count: Int): List<GameReview> = dbQuery {
        GameReviewTable.selectAll()
            .orderBy(GameReviewTable.id, SortOrder.DESC)
            .limit(count)
            .map {
                GameReview(
                    id = it[GameReviewTable.id].value,
                    userId = it[GameReviewTable.userId],
                    gameId = it[GameReviewTable.gameId],
                    rating = it[GameReviewTable.rating],
                    text = it[GameReviewTable.text],
                    likes = it[GameReviewTable.likes]
                )
            }
    }

    suspend fun getTrendingReviews(count: Int): List<GameReview> = dbQuery {
        GameReviewTable.selectAll()
            .orderBy(GameReviewTable.likes, SortOrder.DESC)
            .limit(count)
            .map {
                GameReview(
                    id = it[GameReviewTable.id].value,
                    userId = it[GameReviewTable.userId],
                    gameId = it[GameReviewTable.gameId],
                    rating = it[GameReviewTable.rating],
                    text = it[GameReviewTable.text],
                    likes = it[GameReviewTable.likes]
                )
            }
    }

    suspend fun getTrendingGames(count: Int) = dbQuery {
        // Query para contar o número de avaliações por jogo
        val gameReviewCounts = GameReviewTable
            .slice(GameReviewTable.gameId, GameReviewTable.gameId.count())
            .selectAll()
            .groupBy(GameReviewTable.gameId)
            .orderBy(GameReviewTable.gameId.count(), SortOrder.DESC)
            .limit(count)
            .map {
                it[GameReviewTable.gameId] to it[GameReviewTable.gameId.count()]
            }

        val gameIds = gameReviewCounts.map { it.first }

        gameIds
    }

    suspend fun getAverageRatingByGameId(gameId: Long): Int = dbQuery {
        // Seleciona os ratings do jogo específico
        val ratings = GameReviewTable
            .slice(GameReviewTable.rating)
            .select { GameReviewTable.gameId eq gameId }
            .map { it[GameReviewTable.rating] }

        // Calcula a média se houver avaliações
        if (ratings.isNotEmpty()) {
            // Converte os ratings para Double para calcular a média e arredonda para o inteiro mais próximo
            ratings.map { it.toDouble() }.average().roundToInt()
        } else {
            // Se não houver avaliações, retorna 0
            0
        }
    }

    suspend fun getGameWithAdditionalData(gameId: Long) = dbQuery {
        // Calcular a média de avaliação
        val ratings = GameReviewTable
            .slice(GameReviewTable.rating)
            .select { GameReviewTable.gameId eq gameId }
            .map { it[GameReviewTable.rating] }

        val averageRating = if (ratings.isNotEmpty()) {
            ratings.map { it.toDouble() }.average().roundToInt()
        } else {
            0
        }

        // Contar o número de avaliações
        val reviewCount = GameReviewTable
            .select { GameReviewTable.gameId eq gameId }
            .count()

        Pair(averageRating, reviewCount)
    }

    suspend fun updateGameReview(id: Int, gameReviewCreation: GameReviewCreation): Boolean = dbQuery {
        val updatedRows = GameReviewTable.update({ GameReviewTable.id eq id }) {
            it[userId] = gameReviewCreation.userId
            it[gameId] = gameReviewCreation.gameId
            it[rating] = gameReviewCreation.rating
            it[text] = gameReviewCreation.text
            it[likes] = gameReviewCreation.likes
        }
        updatedRows > 0
    }


    suspend fun deleteGameReview(id: Int): Boolean = dbQuery {
        val deletedRows = GameReviewTable.deleteWhere { GameReviewTable.id eq id }
        deletedRows > 0
    }
}
