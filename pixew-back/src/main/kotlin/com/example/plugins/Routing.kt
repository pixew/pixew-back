package com.example.plugins

import com.example.api.IGDBApiClient
import com.example.data.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import com.example.factory.DatabaseFactory
import com.example.models.*
import io.ktor.server.plugins.cors.routing.*
import io.ktor.server.request.*

// Conversion functions
suspend fun convertGameReviewToResponse(gameReview: GameReview, userRepository: UserRepository, game: Game): GameReviewResponse {
    return GameReviewResponse(
        gameReview.id,
        userRepository.getUserById(gameReview.userId),
        game,
        gameReview.rating,
        gameReview.text,
        gameReview.likes
    )
}

suspend fun convertGameReviewToResponse(gameReview: GameReview, userRepository: UserRepository, igdbApiClient: IGDBApiClient): GameReviewResponse {
    return GameReviewResponse(
        gameReview.id,
        userRepository.getUserById(gameReview.userId),
        igdbApiClient.getGameById(gameReview.gameId),
        gameReview.rating,
        gameReview.text,
        gameReview.likes
    )
}

suspend fun convertGameReviewToResponse(gameReview: GameReview, userRepository: UserRepository): GameReviewResponse {
    return GameReviewResponse(
        gameReview.id,
        userRepository.getUserById(gameReview.userId),
        null,
        gameReview.rating,
        gameReview.text,
        gameReview.likes
    )
}

suspend fun convertGameToGameAdditionalData(game: Game, averageRating: Int, reviewCount: Long): GameWithAdditionalData {
    return GameWithAdditionalData(
        game.id,
        game.name,
        game.summary,
        game.coverUrl,
        game.screenshotUrl,
        averageRating,
        reviewCount
    )
}

suspend fun sortGames(sortedIds: List<Long>, games: List<Game>): List<Game> {
    val orderedGames = mutableListOf<Game>()

    for (id in sortedIds) {
        for (game in games) {
            if (game.id == id) {
                orderedGames.add(game)
                break
            }
        }
    }
    return orderedGames
}

fun Application.configureRouting() {
    install(CORS) {
        anyHost() // Allows requests from any host
        allowMethod(HttpMethod.Post)
        allowHeader(HttpHeaders.ContentType)
        allowNonSimpleContentTypes = true
    }

    val gameReviewRepository: GameReviewRepository = GameReviewRepository()
    val userRepository: UserRepository = UserRepository()
    val playedRepository: PlayedRepository = PlayedRepository()
    val favoritesRepository: FavoritesRepository = FavoritesRepository()

    val clientId = "pls0dgmzbcffvadblxfaifvf3s1e7g"
    val clientSecret = "l0k8wb3t9iyly7zr2dhcsbx6wkgxoa"
    val igdbClient = IGDBApiClient(clientId, clientSecret)

    DatabaseFactory.init()

    routing {
        get("/") {
            call.respondText("Hello, Pixew!")
        }

        // ROTAS PARA USERS //
        get("/users") {
            call.respond(userRepository.getAllUsers())
        }

        get("/users/{id}") {
            val userId = call.parameters["id"]?.toIntOrNull()
            if (userId == null) {
                call.respond(HttpStatusCode.BadRequest, "userId parameter has to be a number")
                return@get
            }
            val user = userRepository.getUserById(userId)
            if (user != null) {
                call.respond(user)
            } else {
                call.respond(HttpStatusCode.NotFound, "User specified not found")
            }
        }

        get("/users/username/{username}") {
            val username = call.parameters["username"]
            if (username.isNullOrBlank()) {
                call.respond(HttpStatusCode.BadRequest, "username parameter cannot be blank")
                return@get
            }

            val user = userRepository.getUserByUsername(username)
            if (user != null) {
                call.respond(user)
            } else {
                call.respond(HttpStatusCode.NotFound, "User with username '$username' not found")
            }
        }

        post("/users") {
            val user = call.receive<UserCreation>()
            val createdUser = userRepository.addUser(user)
            call.respond(HttpStatusCode.Created, createdUser)
        }

        post("/users/batch") {
            val userCreations = call.receive<List<UserCreation>>()
            val addedUsers = userRepository.addUsers(userCreations)
            call.respond(HttpStatusCode.Created, addedUsers)
        }

        put("/users/{id}") {
            val user = call.receive<UserCreation>()
            val userId = call.parameters["id"]?.toIntOrNull()
            if (userId == null) {
                call.respond(HttpStatusCode.BadRequest, "userId parameter has to be a number")
                return@put
            }
            val updatedUser = userRepository.updateUser(userId, user)
            if (updatedUser) {
                call.respond(HttpStatusCode.OK)
            } else {
                call.respond(HttpStatusCode.NotFound, "Found no user with id $userId")
            }
        }

        delete("/users/{id}") {
            val userId = call.parameters["id"]?.toIntOrNull()
            if (userId == null) {
                call.respond(HttpStatusCode.BadRequest, "userId parameter has to be a number")
                return@delete
            }

            val removedUser = userRepository.deleteUser(userId)
            if (removedUser) {
                call.respond(HttpStatusCode.OK)
            } else {
                call.respond(HttpStatusCode.NotFound, "Found no user with id $userId")
            }
        }

        // ROTAS PARA GAME REVIEWS //
        get("/reviews") {
            call.respond(gameReviewRepository.getAllGameReviews().map { gameReview -> convertGameReviewToResponse(gameReview, userRepository, igdbClient) })
        }

        get("/reviews/{id}") {
            val reviewId = call.parameters["id"]?.toIntOrNull()
            if (reviewId == null) {
                call.respond(HttpStatusCode.BadRequest, "reviewId parameter has to be a number")
                return@get
            }
            val review = gameReviewRepository.getGameReviewById(reviewId)
            if (review != null) {
                val reviewResponse = convertGameReviewToResponse(review, userRepository, igdbClient)
                call.respond(reviewResponse)
            } else {
                call.respond(HttpStatusCode.NotFound, "Review specified not found")
            }
        }

        get("/reviews/user/{userId}") {
            val userId = call.parameters["userId"]?.toIntOrNull()
            if (userId == null) {
                call.respond(HttpStatusCode.BadRequest, "userId parameter has to be a number")
                return@get
            }
            val reviews = gameReviewRepository.getReviewsByUserId(userId)
            if (reviews.isNotEmpty()) {
                val gameIds = reviews.map { review -> review.gameId }
                val games = igdbClient.getSpecificGames(gameIds)
                val sortedGames = sortGames(gameIds, games)
                call.respond(reviews.mapIndexed { index, review -> convertGameReviewToResponse(review, userRepository, sortedGames[index]) })
            } else {
                call.respond(HttpStatusCode.NotFound, "No reviews found for the specified user")
            }
        }

        get("/reviews/game/{gameId}") {
            val gameId = call.parameters["gameId"]?.toLongOrNull()
            if (gameId == null) {
                call.respond(HttpStatusCode.BadRequest, "gameId parameter has to be a number")
                return@get
            }
            val reviews = gameReviewRepository.getReviewsByGameId(gameId)
            if (reviews.isNotEmpty()) {
                call.respond(reviews.map { review -> convertGameReviewToResponse(review, userRepository) })
            } else {
                call.respond(HttpStatusCode.NotFound, "No reviews found for the specified game")
            }
        }

        get("/reviews/new/{count}") {
            val count = call.parameters["count"]?.toIntOrNull()
            if (count == null) {
                call.respond(HttpStatusCode.BadRequest, "count parameter has to be a number")
                return@get
            }
            val reviews = gameReviewRepository.getNewReviews(count)
            if (reviews.isNotEmpty()) {
                val gameIds = reviews.map { review -> review.gameId }
                val games = igdbClient.getSpecificGames(gameIds)
                val sortedGames = sortGames(gameIds, games)
                call.respond(reviews.mapIndexed { index, review -> convertGameReviewToResponse(review, userRepository, sortedGames[index]) })
            } else {
                call.respond(HttpStatusCode.NotFound, "No reviews found for the specified user")
            }
        }

        get("/reviews/trending/{count}") {
            val count = call.parameters["count"]?.toIntOrNull()
            if (count == null) {
                call.respond(HttpStatusCode.BadRequest, "count parameter has to be a number")
                return@get
            }
            val reviews = gameReviewRepository.getTrendingReviews(count)
            if (reviews.isNotEmpty()) {
                val gameIds = reviews.map { review -> review.gameId }
                val games = igdbClient.getSpecificGames(gameIds)
                val sortedGames = sortGames(gameIds, games)
                call.respond(reviews.mapIndexed { index, review -> convertGameReviewToResponse(review, userRepository, sortedGames[index]) })
            } else {
                call.respond(HttpStatusCode.NotFound, "No reviews found for the specified user")
            }
        }

        get("/reviews/rating/{gameId}") {
            val gameId = call.parameters["gameId"]?.toLongOrNull()
            if (gameId == null) {
                call.respond(HttpStatusCode.BadRequest, "count parameter has to be a number")
                return@get
            }

            try {
                val averageRating = gameReviewRepository.getAverageRatingByGameId(gameId)
                call.respond(mapOf("averageRating" to averageRating))
            } catch (e: Exception) {
                call.respond(HttpStatusCode.InternalServerError, "An error occurred while fetching the average rating")
            }
        }

        post("/reviews") {
            val review = call.receive<GameReviewCreation>()
            val createdGameReview = gameReviewRepository.addGameReview(review)
            call.respond(HttpStatusCode.Created, createdGameReview)
        }

        post("/reviews/batch") {
            val gameReviewCreations = call.receive<List<GameReviewCreation>>()
            val addedGameReviews = gameReviewRepository.addGameReviews(gameReviewCreations)
            call.respond(HttpStatusCode.Created, addedGameReviews)
        }

        put("/reviews/{id}") {
            val review = call.receive<GameReviewCreation>()
            val reviewId = call.parameters["id"]?.toIntOrNull()
            if (reviewId == null) {
                call.respond(HttpStatusCode.BadRequest, "reviewId parameter has to be a number")
                return@put
            }
            val updatedReview = gameReviewRepository.updateGameReview(reviewId, review)
            if (updatedReview) {
                call.respond(HttpStatusCode.OK)
            } else {
                call.respond(HttpStatusCode.NotFound, "Found no game review with id $reviewId")
            }
        }

        delete("/reviews/{id}") {
            val reviewId = call.parameters["id"]?.toIntOrNull()
            if (reviewId == null) {
                call.respond(HttpStatusCode.BadRequest, "reviewId parameter has to be a number")
                return@delete
            }
            val removedReview = gameReviewRepository.deleteGameReview(reviewId)
            if (removedReview) {
                call.respond(HttpStatusCode.OK)
            } else {
                call.respond(HttpStatusCode.NotFound, "Found no game review with id $reviewId")
            }
        }

        // ROTAS PARA GAMES //
        get("/games") {
            val query = call.request.queryParameters["query"]
            if (query.isNullOrBlank()) {
                call.respond(HttpStatusCode.BadRequest, "Missing query parameter 'query'")
                return@get
            }

            try {
                val games = igdbClient.searchGames(query)
                val gamesWithAdditionalData = games.map { game ->  convertGameToGameAdditionalData(game,
                    gameReviewRepository.getGameWithAdditionalData(game.id).first, gameReviewRepository.getGameWithAdditionalData(game.id).second) }
                call.respond(gamesWithAdditionalData)
            } catch (e: Exception) {
                call.respond(HttpStatusCode.InternalServerError, "An error occurred while fetching games")
            }
        }

        get("/games/{id}") {
            val gameId = call.parameters["id"]?.toLongOrNull()
            if (gameId == null) {
                call.respond(HttpStatusCode.BadRequest, "Invalid or missing game ID")
                return@get
            }
            val game = igdbClient.getGameById(gameId)
            if (game != null) {
                call.respond(game)
            } else {
                call.respond(HttpStatusCode.NotFound, "Game not found")
            }
        }

        get("/games/trending/{count}") {
            val count = call.parameters["count"]?.toIntOrNull()
            if (count == null) {
                call.respond(HttpStatusCode.BadRequest, "count parameter has to be a number")
                return@get
            }


            val gameIds = gameReviewRepository.getTrendingGames(count)
            if (gameIds.isNotEmpty()) {
                var games = igdbClient.getSpecificGames(gameIds)
                games = sortGames(gameIds, games)
                call.respond(games)
            } else {
                call.respond(HttpStatusCode.NotFound, "No trending games found")
            }
        }

        get("/games/additional/{gameId}") {
            val gameId = call.parameters["gameId"]?.toLongOrNull()
            if (gameId == null) {
                call.respond(HttpStatusCode.BadRequest, "Invalid or missing game ID")
                return@get
            }
            val pair = gameReviewRepository.getGameWithAdditionalData(gameId)
            val game = igdbClient.getGameById(gameId)
            if (game != null) {
                val gameWithAdditionalData = convertGameToGameAdditionalData(game, pair.first, pair.second)
                call.respond(gameWithAdditionalData)
            } else {
                call.respond(HttpStatusCode.NotFound, "Game not found")
            }

        }

        get("/games/additional/trending/{count}") {
            val count = call.parameters["count"]?.toIntOrNull()
            if (count == null) {
                call.respond(HttpStatusCode.BadRequest, "count parameter has to be a number")
                return@get
            }

            val gameIds = gameReviewRepository.getTrendingGames(count)
            if (gameIds.isNotEmpty()) {
                val games = igdbClient.getSpecificGames(gameIds)
                val sortedGames = sortGames(gameIds, games)
                val gamesWithAdditionalData = sortedGames.map { game ->
                    val (averageRating, reviewCount) = gameReviewRepository.getGameWithAdditionalData(game.id)
                    convertGameToGameAdditionalData(game, averageRating, reviewCount)
                }
                call.respond(gamesWithAdditionalData)
            } else {
                call.respond(HttpStatusCode.NotFound, "No trending games found")
            }

        }

        // Rotas para Played
        route("/played") {
            post {
                val played = call.receive<Played>()
                val newPlayed = playedRepository.addPlayed(played)
                call.respond(HttpStatusCode.Created, newPlayed)
            }

            delete {
                val played = call.receive<Played>()
                val userId = played.userId
                val gameId = played.gameId

                val deleted = playedRepository.deletePlayed(userId, gameId)
                if (deleted) {
                    call.respond(HttpStatusCode.OK)
                } else {
                    call.respond(HttpStatusCode.NotFound)
                }
            }

            get("/user/{userId}") {
                val userId = call.parameters["userId"]?.toIntOrNull()
                if (userId == null) {
                    call.respond(HttpStatusCode.BadRequest, "Invalid or missing userId")
                    return@get
                }
                val played = playedRepository.getPlayedByUserId(userId)
                call.respond(HttpStatusCode.OK, played)
            }

            get("/game/{gameId}") {
                val gameId = call.parameters["gameId"]?.toLongOrNull()
                if (gameId == null) {
                    call.respond(HttpStatusCode.BadRequest, "Invalid or missing gameId")
                    return@get
                }
                val played = playedRepository.getPlayedByGameId(gameId)
                call.respond(HttpStatusCode.OK, played)
            }

            get {
                val played = playedRepository.getAllPlayed()
                call.respond(HttpStatusCode.OK, played)
            }
        }

        // Rotas para Favorites
        route("/favorites") {
            post {
                val favorite = call.receive<Favorites>()
                val newFavorite = favoritesRepository.addFavorite(favorite)
                call.respond(HttpStatusCode.Created, newFavorite)
            }

            delete {
                val favorite = call.receive<Favorites>()
                val userId = favorite.userId
                val gameId = favorite.gameId

                val deleted = favoritesRepository.deleteFavorite(userId, gameId)
                if (deleted) {
                    call.respond(HttpStatusCode.OK)
                } else {
                    call.respond(HttpStatusCode.NotFound)
                }
            }

            get("/user/{userId}") {
                val userId = call.parameters["userId"]?.toIntOrNull()
                if (userId == null) {
                    call.respond(HttpStatusCode.BadRequest, "Invalid or missing userId")
                    return@get
                }
                val favorite = favoritesRepository.getFavoriteByUserId(userId)
                call.respond(HttpStatusCode.OK, favorite)
            }

            get("/game/{gameId}") {
                val gameId = call.parameters["gameId"]?.toLongOrNull()
                if (gameId == null) {
                    call.respond(HttpStatusCode.BadRequest, "Invalid or missing gameId")
                    return@get
                }
                val favorite = favoritesRepository.getFavoriteByGameId(gameId)
                call.respond(HttpStatusCode.OK, favorite)
            }

            get {
                val favorites = favoritesRepository.getAllFavorites()
                call.respond(HttpStatusCode.OK, favorites)
            }
        }
    }
}
