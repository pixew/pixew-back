package com.example.tables

import com.example.models.GameReview
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object GameReviewTable: IntIdTable("game_review") {
    val userId = integer("userId").references(UserTable.id)
    val gameId = long("gameId")
    val rating = integer("rating")
    val text = text("text")
    val likes = integer("likes")
}

class GameReviewDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<GameReviewDAO>(GameReviewTable)

    var userId by GameReviewTable.userId
    var gameId by GameReviewTable.gameId
    var rating by GameReviewTable.rating
    var text by GameReviewTable.text
}

