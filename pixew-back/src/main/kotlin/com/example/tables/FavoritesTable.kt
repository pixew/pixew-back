package com.example.tables

import org.jetbrains.exposed.sql.Table

object FavoritesTable : Table("played") {
    val userId = integer("userId").references(UserTable.id)
    val gameId = long("gameId")
}