package com.example.tables

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Table

object GameTable: Table("game") {
    val gameId = long("id")
    val name = varchar("name", 512)
    val summary = varchar("summary", 512)
    val coverUrl = varchar("coverUrl", 512)
    val screenshotUrl = varchar("screenshotUrl", 512)
}