package com.example.tables

import com.example.models.User
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.ResultRow
import org.jetbrains.exposed.sql.Table

object UserTable: IntIdTable("user") {
    val userName = varchar("userName", 512)
    val password = varchar("password", 512)
    val displayName = varchar("displayName", 512)
}

class UserDAO(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<UserDAO>(UserTable)

    var userName by UserTable.userName
    var password by UserTable.password
    var displayName by UserTable.displayName
}