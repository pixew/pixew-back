package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class GameReviewDraft (
    val userId: Int,
    val gameId: String,
    val rating: Int,
    val text: String
)