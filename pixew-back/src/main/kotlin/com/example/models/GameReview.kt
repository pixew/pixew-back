package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class GameReview (
    val id: Int,
    val userId: Int,
    val gameId: Long,
    val rating: Int,
    val text: String,
    val likes: Int
)

@Serializable
data class GameReviewCreation (
    val userId: Int,
    val gameId: Long,
    val rating: Int,
    val text: String,
    val likes: Int
)

@Serializable
data class GameReviewResponse (
    val id: Int,
    val user: User?,
    val game: Game?,
    val rating: Int,
    val text: String,
    val likes: Int
)
