package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Favorites (
    val userId: Int,
    val gameId: Long
)