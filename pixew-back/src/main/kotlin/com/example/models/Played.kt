package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Played (
    val userId: Int,
    val gameId: Long
)