package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class UserDraft (
    val userName: String,
    val password: String,
    val displayName: String
)