package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Game (
    val id: Long,
    val name: String,
    val summary: String,
    val coverUrl: String?,
    val screenshotUrl: String?
)

@Serializable
data class GameWithAdditionalData(
    val id: Long,
    val name: String,
    val summary: String,
    val coverUrl: String?,
    val screenshotUrl: String?,
    val averageRating: Int,
    val reviewCount: Long
)