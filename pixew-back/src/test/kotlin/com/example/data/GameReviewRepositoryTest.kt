package com.example.data

import com.example.factory.DatabaseFactory
import com.example.models.GameReviewCreation
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test

class GameReviewRepositoryTest {

    private lateinit var gameReviewRepository: GameReviewRepository

    companion object {
        @BeforeAll
        @JvmStatic
        fun setUp() {
            DatabaseFactory.init()
        }
    }

    @BeforeEach
    fun init() {
        gameReviewRepository = GameReviewRepository()
    }

    @Test
    fun `test add game review`() {
        val gameReviewCreation = GameReviewCreation(
            userId = 4,
            gameId = 100,
            rating = 5,
            text = "Great game!",
            likes = 10
        )

        val addedReview = runBlocking {
            gameReviewRepository.addGameReview(gameReviewCreation)
        }

        val retrievedReview = runBlocking {
            gameReviewRepository.getGameReviewById(addedReview.id)
        }

        assertEquals(gameReviewCreation.userId, retrievedReview?.userId)
        assertEquals(gameReviewCreation.gameId, retrievedReview?.gameId)
        assertEquals(gameReviewCreation.rating, retrievedReview?.rating)
        assertEquals(gameReviewCreation.text, retrievedReview?.text)
        assertEquals(gameReviewCreation.likes, retrievedReview?.likes)

        runBlocking { gameReviewRepository.deleteGameReview(addedReview.id) }
    }

    @Test
    fun `test get game review by id`() {
        val gameReviewCreation = GameReviewCreation(
            userId = 3,
            gameId = 101,
            rating = 4,
            text = "Good game!",
            likes = 20
        )

        val addedReview = runBlocking {
            gameReviewRepository.addGameReview(gameReviewCreation)
        }

        val retrievedReview = runBlocking {
            gameReviewRepository.getGameReviewById(addedReview.id)
        }

        assertNotNull(retrievedReview)
        assertEquals(gameReviewCreation.userId, retrievedReview?.userId)
        assertEquals(gameReviewCreation.gameId, retrievedReview?.gameId)
        assertEquals(gameReviewCreation.rating, retrievedReview?.rating)
        assertEquals(gameReviewCreation.text, retrievedReview?.text)
        assertEquals(gameReviewCreation.likes, retrievedReview?.likes)

        runBlocking { gameReviewRepository.deleteGameReview(addedReview.id) }
    }

    @Test
    fun `test update game review`() {
        val gameReviewCreation = GameReviewCreation(
            userId = 3,
            gameId = 102,
            rating = 3,
            text = "Average game.",
            likes = 5
        )

        val addedReview = runBlocking {
            gameReviewRepository.addGameReview(gameReviewCreation)
        }

        val updatedGameReviewCreation = GameReviewCreation(
            userId = 3,
            gameId = 102,
            rating = 4,
            text = "Better than average.",
            likes = 15
        )

        val updateResult = runBlocking {
            gameReviewRepository.updateGameReview(addedReview.id, updatedGameReviewCreation)
        }

        assertTrue(updateResult)

        val retrievedReview = runBlocking {
            gameReviewRepository.getGameReviewById(addedReview.id)
        }

        assertNotNull(retrievedReview)
        assertEquals(updatedGameReviewCreation.rating, retrievedReview?.rating)
        assertEquals(updatedGameReviewCreation.text, retrievedReview?.text)
        assertEquals(updatedGameReviewCreation.likes, retrievedReview?.likes)

        runBlocking { gameReviewRepository.deleteGameReview(addedReview.id) }
    }

    @Test
    fun `test delete game review`() {
        val gameReviewCreation = GameReviewCreation(
            userId = 4,
            gameId = 103,
            rating = 2,
            text = "Not great.",
            likes = 2
        )

        val addedReview = runBlocking {
            gameReviewRepository.addGameReview(gameReviewCreation)
        }

        val deleteResult = runBlocking {
            gameReviewRepository.deleteGameReview(addedReview.id)
        }

        assertTrue(deleteResult)

        val retrievedReview = runBlocking {
            gameReviewRepository.getGameReviewById(addedReview.id)
        }

        assertNull(retrievedReview)
    }

    @Test
    fun `test get reviews by user id`() {
        val userId = 7
        val gameReviewCreation1 = GameReviewCreation(
            userId = userId,
            gameId = 106,
            rating = 4,
            text = "Pretty good!",
            likes = 12
        )
        val gameReviewCreation2 = GameReviewCreation(
            userId = userId,
            gameId = 107,
            rating = 2,
            text = "Not my favorite.",
            likes = 1
        )

        runBlocking {
            gameReviewRepository.addGameReview(gameReviewCreation1)
            gameReviewRepository.addGameReview(gameReviewCreation2)
        }

        val reviewsByUserId = runBlocking {
            gameReviewRepository.getReviewsByUserId(userId)
        }

        assertTrue(reviewsByUserId.any { it.gameId == gameReviewCreation1.gameId })
        assertTrue(reviewsByUserId.any { it.gameId == gameReviewCreation2.gameId })

        // Clean up
        runBlocking {
            reviewsByUserId.forEach { review ->
                gameReviewRepository.deleteGameReview(review.id)
            }
        }
    }
}
