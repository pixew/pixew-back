package com.example.data

import com.example.factory.DatabaseFactory
import com.example.models.User
import com.example.models.UserCreation
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class UserRepositoryTest {

    private lateinit var userRepository: UserRepository

    companion object {
        @BeforeAll
        @JvmStatic // Garante compatibilidade com plataformas que requerem métodos estáticos
        fun setUp() {
            DatabaseFactory.init() // Inicializa a conexão com o banco de dados real antes de todos os testes
        }
    }

    @BeforeEach
    fun init() {
        userRepository = UserRepository()
    }

    @Test
    fun `test add user`() {
        val userCreation = UserCreation(
            userName = "hi",
            password = "password",
            displayName = "Test User"
        )

        val addedUser = runBlocking {
            userRepository.addUser(userCreation)
        }

        val retrievedUser = runBlocking {
            userRepository.getUserById(addedUser.id)
        }

        assertEquals(userCreation.userName, retrievedUser?.userName)
        assertEquals(userCreation.password, retrievedUser?.password)
        assertEquals(userCreation.displayName, retrievedUser?.displayName)

        runBlocking { userRepository.deleteUser(addedUser.id) }
    }

    @Test
    fun `test get user by username`() {
        val userCreation = UserCreation(
            userName = "uniqueUser",
            password = "password",
            displayName = "Unique User"
        )

        val addedUser = runBlocking {
            userRepository.addUser(userCreation)
        }

        val retrievedUser = runBlocking {
            userRepository.getUserByUsername(userCreation.userName)
        }

        assertEquals(userCreation.userName, retrievedUser?.userName)
        assertEquals(userCreation.password, retrievedUser?.password)
        assertEquals(userCreation.displayName, retrievedUser?.displayName)

        runBlocking { userRepository.deleteUser(addedUser.id) }
    }

    @Test
    fun `test update user`() {
        val userCreation = UserCreation(
            userName = "oldName",
            password = "oldPassword",
            displayName = "Old User"
        )

        val addedUser = runBlocking {
            userRepository.addUser(userCreation)
        }

        val updatedUserCreation = UserCreation(
            userName = "newName",
            password = "newPassword",
            displayName = "New User"
        )

        val updateResult = runBlocking {
            userRepository.updateUser(addedUser.id, updatedUserCreation)
        }

        assertTrue(updateResult)

        val retrievedUser = runBlocking {
            userRepository.getUserById(addedUser.id)
        }

        assertEquals(updatedUserCreation.userName, retrievedUser?.userName)
        assertEquals(updatedUserCreation.password, retrievedUser?.password)
        assertEquals(updatedUserCreation.displayName, retrievedUser?.displayName)

        runBlocking { userRepository.deleteUser(addedUser.id) }
    }

    @Test
    fun `test delete user`() {
        val userCreation = UserCreation(
            userName = "toDelete",
            password = "password",
            displayName = "To Delete"
        )

        val addedUser = runBlocking {
            userRepository.addUser(userCreation)
        }

        val deleteResult = runBlocking {
            userRepository.deleteUser(addedUser.id)
        }

        assertTrue(deleteResult)

        val retrievedUser = runBlocking {
            userRepository.getUserById(addedUser.id)
        }

        assertNull(retrievedUser)
    }
}
